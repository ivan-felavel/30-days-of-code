#coding: utf-8

n =  int (raw_input())
cont = 0
maxi = 0
ans = 1

while n > 0:
	#print n % 2,
	if n % 2 == 1:
		if ans == 1:
			cont += 1
			if maxi <  cont:
				maxi = cont
		else:
			cont = 1
			if maxi <  cont:
				maxi = cont 
		ans = 1
	else: 
		ans = 0
		cont = 0
	n /= 2

print maxi