def solution(fecha_esperada, fecha_dev):
	if fecha_dev[0] <= fecha_esperada[0] and fecha_dev[1] <= fecha_esperada[1] and fecha_dev[2] <= fecha_esperada[2]:
		return 0 
	if fecha_dev[0] > fecha_esperada[0] and fecha_dev[1] == fecha_esperada[1] and fecha_dev[2] == fecha_esperada[2]:
		return 15 * (fecha_dev[0] - fecha_esperada[0])
	if fecha_dev[1] > fecha_esperada[1] and fecha_dev[2] == fecha_esperada[2]:
		return 500 * (fecha_dev[1] - fecha_esperada[1])
	if fecha_esperada[2] < fecha_dev[2]:
		return 10000
	else:
		return 0

fecha_dev = map(int, raw_input().strip().split(' '))
fecha_esperada = map(int, raw_input().strip().split(' '))

print solution(fecha_esperada, fecha_dev)
