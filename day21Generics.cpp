#include <iostream>
#include <vector>

using namespace std;

template <class myType>

void printArray (vector <myType> myVector) {
	for(auto c: myVector)
		cout << c << endl;
}

int main() {
  
    vector<int> vInt{1, 2, 3};
    vector<string> vString{"Hello", "World"};
    
    printArray<int>(vInt);
    printArray<string>(vString);
    
    return 0;
}