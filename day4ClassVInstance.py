#coding: utf-8

class Person(object):
	"""docstring for Person"""
	def __init__(self, initialYear):
		if initialYear < 0:
			print "Age is not valid, setting age to 0."
			self.age = 0
		else:
			self.age =  initialYear

	def yearPasses(self):
		self.age += 1

	def amIOld(self):
		if self.age < 13:
			print "You are young."
		elif 13 <= self.age < 18:
			print "You area a teenager."
		else:
			print "You are old."
t = int(raw_input())
for i in range(0, t):
    age = int(raw_input())         
    p = Person(age)  
    p.amIOld()
    for j in range(0, 3):
        p.yearPasses()        
    p.amIOld()
    print("")