# Enter your code here. Read input from STDIN. Print output to STDOUT
mealCost = float(raw_input())
tipPercent = float(raw_input())
taxPercent =  float(raw_input())

print "The total meal cost is %d dollars." % int(round(mealCost + mealCost * (tipPercent/100) + mealCost * (taxPercent/100)))
