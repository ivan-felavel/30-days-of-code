#coding: utf-8

t = int (raw_input())
for x in range(0,t):
	s = str(raw_input())
	pares = ""
	impares = ""
	for i in range(0,len(s)):
		if i % 2 == 0:
			pares += str(s[i])
	for i in range(0,len(s)):
		if i % 2 != 0:
			impares += str(s[i])
	print "%s %s" %(pares, impares)