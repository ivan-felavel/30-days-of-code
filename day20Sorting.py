#coding : utf-8

import sys


def swap(a , b):
	print "swap"
	temp = a
	a = b
	b = temp
 
n = int(raw_input().strip())
a = map(int, raw_input().strip().split(' '))
totalSwaps = 0

for i in range(0,n):
	numberOfSwaps = 0
	for j in range(0,n-1):
		if a[j] > a[j+1]:
			a[j], a[j+1] = a[j+1], a[j] 	
			numberOfSwaps += 1
			totalSwaps += 1 		
	if numberOfSwaps == 0:
		break

print "Array is sorted in %d swaps." %totalSwaps
print "First element: %d" %a[0]
print "Last element: %d" %a[-1]
