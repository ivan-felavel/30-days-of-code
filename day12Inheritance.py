class Person(object):
	def __init__(self, firstName, lastName, idNumber):
		self.firstName = firstName
		self.lastName = lastName
		self.idNumber = idNumber
	def printPerson(self):
		print "Name:", self.lastName + ",", self.firstName
		print "ID:", self.idNumber

class Student(Person):
	"""docstring for Student"""
	def __init__(self, firstName, lastName, idNum, scores):
		super(Student, self).__init__(firstName, lastName, idNum)
		self.scores = scores

	def calculate(self):
		average = 0
		for x in xrange(0,len(self.scores)):
			average += self.scores[x]
		average /= len(self.scores)
		if 90 <= average <= 100:
			return 'O'
		elif 80 <= average < 90:
			return 'E'
		elif 70 <= average < 80:
			return 'A'
		elif 55 <= average < 70:
			return 'P'
		elif 40 <= average < 55:
			return 'D'
		else:
			return 'T' 


line = raw_input().split()
firstName = line[0]
lastName = line[1]
idNum = line[2]
numScores = int(raw_input()) # not needed for Python
scores = map(int, raw_input().split())
s = Student(firstName, lastName, idNum, scores)
s.printPerson()
print "Grade:", s.calculate()