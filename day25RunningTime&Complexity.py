def is_prime(n):
	if n <= 1:
		return False
	elif n <= 3:
		return True
	elif n % 2 == 0 or n % 3 == 0:
		return False

	x = 5

	while x * x <= n :
		if n % x == 0 or n % (x +2) == 0:
			return False
		x += 6

	return True

t = int(raw_input())
for x in range(0,t):
	n = int(raw_input())
	if is_prime(n):
		print "Prime"
	else:
		print "Not prime"